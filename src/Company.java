public class Company extends Customer {

    private int taxNumber;

    public Company(String name, String email, int taxNumber) {
        super(name, email);
        this.taxNumber = taxNumber;
    }

    @Override
    public int GetIdentificationNumber() {
        return taxNumber;
    }

}
