public class SingularPerson extends Customer {

    private int taxNumber;

    public SingularPerson(String name, String email, int taxNumber) {
        super(name, email);
        this.taxNumber = taxNumber;
    }

    @Override
    public int GetIdentificationNumber() {
        return taxNumber;
    }

}