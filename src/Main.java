public class Main {
    public static void main(String[] args) {
        SingularPerson rui = new SingularPerson("Rui Godinho", "rui@gmail.com", 191919191);
        Company securibox = new Company("Securibox", "securibox@securibox.com", 111110099);

        System.out.println("* * * * * * * * * * * * *");
        rui.setEmail("ruigodinho90@gmail.com");
        System.out.println("Name: " + rui.getName());
        System.out.println("Email: " + rui.getEmail());
        System.out.println("NIF: " + rui.GetIdentificationNumber());
        System.out.println("* * * * * * * * * * * * *");
        System.out.println("Name: " + securibox.getName());
        System.out.println("Email: " + securibox.getEmail());
        System.out.println("NIPC: " + securibox.GetIdentificationNumber());
    }
}