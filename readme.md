Bonus Question:

For each Singular person, there is a list of purchases (Identification of the purchased
product, unitary price without tax, quantity, tax rate and the reference of the company
it has been bought from).
For each company, there is a list of their sellings (Identification of the product sold,
unitary price without tax, quantity, tax rate and the reference to which Singular person
it has been sold to).
We wish to perform a check of consistency of the data.
Could you propose a way to solve this problem? It does not need to be extensive, but
it needs to show how you solve this problem in a logical way.


My answer:

I believe that what we have to do is to create some database. From my experience at Academia de Código, we have used
JDBC and Spring, and later we learned how to use Hibernate - (created some beans firstly, with a lot of XML configs and 
then annotations). All this with MySql.

Based on the ability to map Java Objects (POJOs) into Databases, we needed to add some more proprieties and methods to our classes also,
create a new one: Product.
By performing CRUD operations we can use a Relatable DataBase Managing System to check the consistency of the data.

So, for this exercise we need to create 3 tables (SinglePerson, Company and Product).
SinglePerson would have an ID_Person (Primary key), Name, Email, NIF and ID_Product (foreign key). 
Company would have an ID_Company (Primary key), Name, Email, NIPC and ID_Product (foreign key).
Product would have an ID_Product (Primary key), Product_Type, Unit_Price, Quantity, Tax_Rate and Transaction
(from the SinglePerson or the Company), that would be the foreign key.

If we use some logic for the Transaction field we can check by the ID what type of Transaction was made: bought for ID_SinglePerson,
or selling for the ID_Company. With this information we can also update the values from the Quantity.

The main idea here, is to connect all tables, using the ID's so that when we want to check who bought or sold some products we
can print the info: ex.: ID_Person #1, name Rui, email rui@com, ID_Product #100, Product_Type shoes, Unit_Price 50€, 
Tax_Rate 10%, Transaction Bought

With this, we could manipulate the values based on the information we store at the database and check the consistency of 
the data.

